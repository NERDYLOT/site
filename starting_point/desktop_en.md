## Desktop Environments

There are different environments you can use on your linux installation. They are the following:

* KDE Plasma
* GNOME
* Cinnamon
* MATE
* XFCE
* Budgie
* LXDE
