+++
categories = ["Introduction"]
date = "2017-06-07T12:01:58-04:00"
tags = []
title = "red_hat"

+++
# Red Hat Enterprise Linux


<b>Overview</b>
Red Hat Enterprise Linux commonly referred to as RHEL is developed by Red Hat. RHEL has strict rules to restrict free re-distribution although it still provides source code for free.

<b>Package Management</b>
RHEL uses yum for package management.

<b>Configurability</b>
RHEL-based operating systems will differ slightly from the Debian-based operating systems, most noticeably in package management. If you decide to go with RHEL it’s probably best if you know you’ll be working with it.

<b>Uses</b>
As described by the name it's mostly used in enterprise, so if you need a solid server OS this would be a good one.
