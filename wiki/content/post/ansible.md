+++
categories = ["Ansible"]
date = "2017-06-10T00:33:01-04:00"
tags = []
title = "Ansible"

+++



Ansible is a configuration management tool written in python and uses SSH to configure hosts. Unlike other tools, it does not require an agent to be able to work. Ansible uses the concept of playbooks and roles. A role are tasks that will be accomplished on the host and playbooks are a collection of roles and list of hosts. Playbooks are written in YAML files. For example:

* A role can be used to install postgresql, initiate the database and start the service.
* A playbook can have a group of hosts, a list of roles and other variables.

## Install Ansible
```
pip install -U ansible
```
```
sudo apt install ansible
```
```
sudo yum install epel-release
sudo yum install ansible
```
